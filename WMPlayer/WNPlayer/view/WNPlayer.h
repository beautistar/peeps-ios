//
//  WNPlayerFrame.h
//  PlayerDemo
//
//  Created by zhengwenming on 2018/10/15.
//  Copyright © 2018年 DS-Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WNPlayerManager.h"

#import <MediaPlayer/MediaPlayer.h>
#import "WMPlayerModel.h"
#import "FastForwardView.h"
#import "WMLightView.h"


typedef enum : NSUInteger {
    WNPlayerStatusNone,
    WNPlayerStatusOpening,
    WNPlayerStatusOpened,
    WNPlayerStatusPlaying,
    WNPlayerStatusBuffering,
    WNPlayerStatusPaused,
    WNPlayerStatusEOF,
    WNPlayerStatusClosing,
    WNPlayerStatusClosed,
} WNPlayerStatus;

//手势操作的类型
typedef NS_ENUM(NSUInteger,WMControlType) {
    WMControlTypeDefault,//无任何操作
    WMControlTypeProgress,//视频进度调节操作
    WMControlTypeVoice,//声音调节操作
    WMControlTypeLight//屏幕亮度调节操作
} ;



@class WNPlayer;
@protocol WNPlayerDelegate <NSObject>
@optional
//点击播放暂停按钮代理方法
-(void)wnplayer:(WNPlayer *)wnplayer clickedPlayOrPauseButton:(UIButton *)playOrPauseBtn;
//点击关闭按钮代理方法
-(void)wnplayer:(WNPlayer *)wnplayer clickedCloseButton:(UIButton *)backBtn;
//点击全屏按钮代理方法
-(void)wnplayer:(WNPlayer *)wnplayer clickedFullScreenButton:(UIButton *)fullScreenBtn;
//单击WMPlayer的代理方法
-(void)wnplayer:(WNPlayer *)wnplayer singleTaped:(UITapGestureRecognizer *)singleTap;

//播放失败的代理方法
-(void)wnplayerFailedPlay:(WNPlayer *)wnplayer WNPlayerStatus:(WNPlayerStatus)state;
//播放器已经拿到视频的尺寸大小
-(void)wnplayerGotVideoSize:(WNPlayer *)wnplayer videoSize:(CGSize )presentationSize;
//播放完毕的代理方法
-(void)wnplayerFinishedPlay:(WNPlayer *)wnplayer;
@end



NS_ASSUME_NONNULL_BEGIN

@interface WNPlayer : UIView

@property (nonatomic, strong) WNPlayerManager *playerManager;
@property (nonatomic,copy) NSString *urlString;
@property (nonatomic,copy) NSString *title;
@property (nonatomic, weak)id <WNPlayerDelegate> delegate;
// 播放器着色
@property (nonatomic,strong) UIColor *tintColor;
@property (nonatomic,assign) BOOL autoplay;
@property (nonatomic,assign) BOOL isFullscreen;
@property (nonatomic,assign) BOOL repeat;
@property (nonatomic,assign) BOOL preventFromScreenLock;
@property (nonatomic,assign) BOOL restorePlayAfterAppEnterForeground;
@property (nonatomic,readonly) WNPlayerStatus status;

// new control added
@property (nonatomic,assign) BOOL  hasMoved;
//总时间
@property (nonatomic,assign)CGFloat totalTime;
//记录触摸开始时的视频播放的时间
@property (nonatomic,assign)CGFloat touchBeginValue;
//记录触摸开始亮度
@property (nonatomic,assign)CGFloat touchBeginLightValue;
//记录触摸开始的音量
@property (nonatomic,assign) CGFloat touchBeginVoiceValue;
//记录touch开始的点
@property (nonatomic,assign) CGPoint touchBeginPoint;
//手势控制的类型,用来判断当前手势是在控制进度?声音?亮度?
@property (nonatomic,assign) WMControlType controlType;
//亮度调节的view
@property (nonatomic,strong) WMLightView * lightView;
//这个用来显示滑动屏幕时的时间
@property (nonatomic,strong) FastForwardView * FF_View;

@property (nonatomic,strong) UISlider   *progressSlider,*volumeSlider;


@property (nonatomic,assign) BOOL  enableVolumeGesture;
/**
 是否开启后台播放模式
 */
@property (nonatomic,assign) BOOL  enableBackgroundMode;
/**
 是否开启快进手势
 */
@property (nonatomic,assign) BOOL  enableFastForwardGesture;

- (void)open;
- (void)close;
- (void)play;
- (void)pause;
//判断是否为iPhone X系列
+(BOOL)IsiPhoneX;
@end

NS_ASSUME_NONNULL_END
