//
//  SideMenuVC.swift
//  Peeps
//
//  Created by Mac on 26/12/18.
//  Copyright © 2018 Kirti. All rights reserved.
//

import UIKit
import Localize_Swift
import SDWebImage

let kHome  = "khome"
let kFavorite  = "kfavorite"
let kSetting  = "ksetting"
let klogout  = "Logout"
let kremotes  = "Remotes"

class SideMenuVC: UIViewController {
  
    var arr_menu = [String]()
      var arr_menuImages = [String]()
      @IBOutlet weak var tbl_menu: UITableView!
      @IBOutlet weak var imgview_Profile: UIImageView!
      @IBOutlet weak var lbl_name: UILabel!
     @IBOutlet weak var imgvw_back: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    arr_menu .append(kHome)
   // arr_menu .append(kremotes)
    arr_menu .append(kFavorite)
    arr_menu .append(kSetting)
    arr_menu .append(klogout)

        
      arr_menuImages .append("menu_home")
    //  arr_menuImages .append("remote")
      arr_menuImages .append("menu_favorite")
      arr_menuImages .append("menu_setting")
      arr_menuImages .append("logout")
     
        
           self.imgvw_back.image = UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!)
        
        let strimgUrl = UserDefaults.standard .value(forKey: kUserProfileUrl) as! String
        
        imgview_Profile?.sd_setImage(with: URL(string: strimgUrl), completed: { (image, error, sdimageCache, url) in
            
        })
        self.imgview_Profile.layer.cornerRadius = self.imgview_Profile.frame.width/2;
        self.imgview_Profile.layer.masksToBounds = true;
        
        lbl_name.text = UserDefaults.standard .value(forKey: kUserName) as? String
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(myFunction), name:.SidemenuUpdate , object: nil)
     
    
    }
    @objc func myFunction(notificaiont: Notification) {
        print(notificaiont.object ?? "") //myObject
        print(notificaiont.userInfo ?? "") //[AnyHashable("key"): "Value"]
         self.tbl_menu .reloadData()
            self.imgvw_back.image = UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!)
    }
}
extension SideMenuVC :UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arr_menu.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        
        let imageview = cell.contentView.viewWithTag(9) as? UIImageView
        imageview?.image = UIImage(named: arr_menuImages[indexPath.row])
        
        let lblName = cell.contentView.viewWithTag(10) as? UILabel
         lblName?.text = arr_menu[indexPath.row].localized()
        lblName?.textAlignment = .left;

        
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arr_menu[indexPath.row] == kFavorite {
            let objchannel = self.storyboard?.instantiateViewController(withIdentifier: "ChannelVC") as! ChannelVC
            objchannel.arr_channelsList = self.getFavoritedata() as! [Channel]
            objchannel.isFromFav = true;
            objchannel.str_title =  "Favorite"
            let navbar = UINavigationController(rootViewController: objchannel)
            panel?.center(navbar)
  
        }
        else if arr_menu[indexPath.row] == kSetting {
            let objSettingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
           
            let navbar = UINavigationController(rootViewController: objSettingsVC)
            panel?.center(navbar)
        }
        else  if arr_menu[indexPath.row] == kHome{
            let centerVC: HomeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let centerNavVC = UINavigationController(rootViewController: centerVC)
            panel?.center(centerNavVC)
        }
        else  if arr_menu[indexPath.row] == kremotes{
            let centerVC: HomeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
           // centerVC.isfromRemote = true;
            let centerNavVC = UINavigationController(rootViewController: centerVC)
            panel?.center(centerNavVC)
        }
        
        else if arr_menu[indexPath.row] == klogout {
            
            let alertController = UIAlertController(title: "Alert", message: "Are you sure want to logout ?", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "YES", style: .default) { (action:UIAlertAction) in
                print("You've pressed default");
                
                UserDefaults.standard .set(false, forKey: klogged)
                UserDefaults.standard .synchronize()
                
                let obj = UIApplication.shared.delegate as! AppDelegate
                obj.window?.rootViewController = self.storyboard?.instantiateInitialViewController()
            }
            
            let action2 = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction) in
                print("You've pressed cancel");
            }
            
            
            
            alertController.addAction(action1)
            alertController.addAction(action2)
           
            self.present(alertController, animated: true, completion: nil)
        }
        
     
//        let objchannel = self.storyboard?.instantiateViewController(withIdentifier: "ChannelVC") as! ChannelVC
//        objchannel.arr_channelsList = filteredArray
//        objchannel.str_title =  channel.tvgroup
//        self.navigationController?.pushViewController(objchannel, animated: false)
    }


    

}
