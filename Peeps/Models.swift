import Foundation
import Alamofire

//struct RawPlaylist {
//    let url: URL
//    let content: String
//}
//http://13.127.134.33

//let kHostpath = "http://project-9.uaecono7.beget.tech/admin/api/"
let kHostpath = "http://13.127.134.33/admin/api/"
let kLogin = "login"
let kRegister = "register"
let kSubmitPlaylist = "submitPlayList"
let kgetPlaylist =  "getPlayList"
let kgetVideolist =  "getVideos"

let ktvShowList = "getTVShowList"
let ktvShowDetailList = "getTVShowContent"
let ktvSetFavorites = "setFavorite"
let ktvgetFavorites = "getFavoriteTVShow"

let kmoviewList = "getMovies"


let klogged =  "klogged1"
let kBannerid = "ca-app-pub-3940256099942544/2934735716"
let kSelectedImage = "kSelectedImage"
let kSelectedLanguage = "kSelectedLanguage"

//MARK: - Constants
struct Constants {
    static let actionFileTypeHeading = "Add a File"
    static let actionFileTypeDescription = "Choose a filetype to add..."
    static let camera = "Camera"
    static let phoneLibrary = "Phone Library"
    static let video = "Video"
    static let file = "File"
    
    
    static let alertForPhotoLibraryMessage = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
    
    static let alertForCameraAccessMessage = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
    
    static let alertForVideoLibraryMessage = "App does not have access to your video. To enable access, tap settings and turn on Video Library Access."
    
    
    static let settingsBtnTitle = "Settings"
    static let cancelBtnTitle = "Cancel"
    
}
struct Channel {
     let title: String
     let tvLogo: String
     let tvgroup: String
     //var url: URL?
     var strurl: String
    
     init(title:String="", tvLogo:String="",tvgroup:String="",strurl:String = "") {
        self.title = title
        self.tvLogo = tvLogo
        self.tvgroup = tvgroup
        self.strurl = strurl
    }
//    init(coder decoder: NSCoder) {
//        self.title = decoder.decodeObject(forKey: "title") as? String ?? ""
//        self.tvLogo = decoder.decodeObject(forKey: "tvLogo") as? String ?? ""
//         self.tvgroup = decoder.decodeObject(forKey: "tvgroup") as? String ?? ""
//         self.strurl = decoder.decodeObject(forKey: "strurl") as? String ?? ""
//    }
    
    
    
    init?(dictionary : [String:String]) {
        guard let title = dictionary["title"],
            let tvLogo = dictionary["tvLogo"],
            let strurl = dictionary["strurl"],
        let tvgroup = dictionary["tvgroup"] else { return nil }
      self.init(title: title as? String ?? "", tvLogo: tvLogo as? String ?? "", tvgroup: tvgroup as? String ?? "", strurl: strurl as? String ?? "")
    }
    
//    func encode(with coder: NSCoder) {
//        coder.encode(title, forKey:"title")
//        coder.encode(tvLogo, forKey:"tvLogo")
//        coder.encode(tvgroup, forKey:"tvgroup")
//        coder.encode(strurl, forKey:"strurl")
//    }
    var propertyListRepresentation : [String:String] {
        return ["title" : title, "tvLogo" : tvLogo, "tvgroup" : tvgroup,"strurl":strurl]
    }

}
struct RemoteFolderList {
    
    var playlist_id: String
    var playlist_name: String
    var created_at: String
    
    init(playlist_id : String, playlist_name : String,created_at : String) {
        self.playlist_id = playlist_id
        self.playlist_name = playlist_name
        self.created_at = created_at
       
    }
}
struct FolderList {
    
    var nameFolder: String
    var pathFolder: String
    var dateCreate: Date
     var pathUrl: String
    
    init(nameFolder : String, pathFolder : String, dateCreate : Date,pathUrl : String) {
        self.nameFolder = nameFolder
        self.pathFolder = pathFolder
        self.dateCreate = dateCreate
        self.pathUrl = pathUrl
    }
    
    init?(dictionary : [String:Any]) {
        guard let nameFolder = dictionary["nameFolder"],
            let pathFolder = dictionary["pathFolder"],
                let pathUrl = dictionary["pathUrl"],
             let dateCreate = dictionary["dateCreate"] else { return nil }
        self.init(nameFolder: nameFolder as? String ?? "", pathFolder: pathFolder as? String ?? "", dateCreate: dateCreate as! Date,pathUrl : pathUrl as? String ?? "")
    }
    
    var propertyListRepresentation : [String:Any] {
        return ["nameFolder" : nameFolder, "pathFolder" : pathFolder, "dateCreate" : dateCreate,"pathUrl" : pathUrl]
    }
}
extension Notification.Name {
    static let SidemenuUpdate = Notification.Name("SidemenuUpdate")
}
extension String {
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
    func isContainText()->Bool
    {
        if self.trimmingCharacters(in: .whitespaces).count > 0 {
            return true
        }
        return false
    }
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func isValidUserName() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^(?=.*[0-9])(?=.*[a-zA-Z]).{5,12}$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func createThumbnailOfVideoFromRemoteUrl() -> UIImage? {
        let asset = AVAsset(url: URL(string: self)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        //assetImgGenerate.maximumSize = CGSize(width,height)
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
extension Array {
    func unique<T:Hashable>(by: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(by(value)) {
                set.insert(by(value))
                arrayOrdered.append(value)
            }
        }
        
        return arrayOrdered
    }
}
extension UIButton {
    func setBorderAndCorner() {
       
        self.layer.cornerRadius = 10
        self.backgroundColor = UIColor.white
        self.layer.masksToBounds = true
        self.titleLabel?.textColor  = UIColor.black
    }
}
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
extension UIViewController
{
    func getFavoritedata()->NSMutableArray
    {
        let arrfull = NSMutableArray(contentsOfFile: self.getfavoritePath())

        let arrr = arrfull?.compactMap{ Channel(dictionary: $0 as! [String : String]) }
        
        return NSMutableArray(array: arrr ?? Array())
    }
    func setbackgroudImage()
    {
         let imageview = UIImageView()
        imageview.contentMode = .scaleToFill;
        imageview.image = UIImage(named: "default")
        self.view.addSubview(imageview)
        self.view .bringSubviewToFront(imageview)
        
    }
     func saveFavoritedata(arr:[Channel])
     {
        
        let propertylistSongs = arr.map{ $0.propertyListRepresentation }
        
        let arrMutable = NSMutableArray(array: propertylistSongs)
        arrMutable .write(toFile: self.getfavoritePath(), atomically: false)
    }
    func getfavoritePath() -> String {
        let plistFileName = "Favorite.plist"
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentPath = paths[0] as NSString
        let plistPath = documentPath.appendingPathComponent(plistFileName)
        return plistPath
    }
    func extractChannelsFromRawString(_ string: String) -> [Channel] {
        var channels = [Channel]()
        string.enumerateLines { line, shouldStop in
            if line.hasPrefix("#EXTINF:") {
                let infoLine = line.replacingOccurrences(of: "#EXTINF:", with: "")
                let infoItems = infoLine.components(separatedBy: ",")
                if let title = infoItems.last {
                    let tvslogo = infoLine.slice(from: "tvg-logo=\"", to: "\"")
                    let tvsgroup = infoLine.slice(from: "group-title=\"", to: "\",")
                    let channel = Channel.init(title: title, tvLogo: tvslogo ?? "", tvgroup: tvsgroup ?? "", strurl: "")
                    channels.append(channel)
                }
            } else {
                if var channel = channels.popLast() {
                    let url = channel.strurl
                    if line.count > 0{
                        channel.strurl = line
                    }else
                    {
                        channel.strurl = url
                    }
                    
                    channels.append(channel)
                    
                }
            }
        }
        return channels
    }
    func showAlertMessage(message:String)
    {
         TinyToast.shared.show(message: message, valign: .center, duration: .normal)
    }
    @objc func openLeftMenu()
    {
        panel?.openLeft(animated: true)
    }
    @objc func backButtonAction()
    {
       self.navigationController?.popViewController(animated: true)
    }
    func setSideMenubtn()
    {
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "menu"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.openLeftMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.leftBarButtonItem = item1
    }
    func setBackbutton()
    {
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "back"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.backButtonAction), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.leftBarButtonItem = item1
    }
}
extension Double {
    func asString(style: DateComponentsFormatter.UnitsStyle) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second, .nanosecond]
        formatter.unitsStyle = style
        guard let formattedString = formatter.string(from: self) else { return "" }
        return formattedString
    }
}
//class Connectivity {
//    class func isConnectedToInternet() ->Bool {
//        return NetworkReachabilityManager()!.isReachable
//    }
//}
