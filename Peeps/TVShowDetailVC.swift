//
//  TVShowDetailVC.swift
//  Peeps
//
//  Created by kirti on 30/04/19.
//  Copyright © 2019 Kirti. All rights reserved.
//

import UIKit
import Alamofire


class TVShowDetailVC: UIViewController {

    @IBOutlet weak var tbl_tvDetail: UITableView!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_seriousName: UILabel!
    var dict_detail = NSDictionary()
  
    var arr_episodeList = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

         self.lbl_seriousName.text = dict_detail .value(forKey: "series_name") as? String
        self.lbl_description.text = dict_detail .value(forKey: "description") as? String
        // Do any additional setup after loading the view.
        self.getTVDetailShowList()
        
        self.tbl_tvDetail.estimatedRowHeight = 150
        self.tbl_tvDetail.rowHeight = UITableView.automaticDimension
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "fav"), style: .plain, target: self, action: #selector(btnActionFavoriteClicked))

    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
   @objc func btnActionFavoriteClicked(){
        self .settFavoriteTVShow()
    }
    
    func getTVDetailShowList()
    {
        let parameters = ["series_id":self.dict_detail .value(forKey: "series_id")!] as [String : AnyObject]
    
        WebserviceHelper.postWebServiceCall(urlString: kHostpath+ktvShowDetailList, parameters:parameters, encodingType: HttpBodyEncoding,ShowProgress:true) { (isSuccess, dictData) in
            if isSuccess == true {
                let dict = dictData .value(forKey: "content_list") as! NSDictionary
                if let array = dict .object(forKey: "season_list") as? NSArray,array.count > 0 {
                    let dict = array[0] as! NSDictionary
                    self.arr_episodeList = NSMutableArray(array: dict .value(forKey: "episode_list") as! NSArray)
                    self.tbl_tvDetail .reloadData()
                }


            }else{
                self.showAlertMessage(message:dictData["message"] as! String)
            }
        }
        
      
        
        
    }
    func settFavoriteTVShow() {
        let parameters = ["series_id":self.dict_detail .value(forKey: "series_id")!,"user_id":UserDefaults.standard.value(forKey: kUserID)!,"mark":"yes"] as  [String : AnyObject]
    
        WebserviceHelper.postWebServiceCall(urlString: kHostpath+ktvSetFavorites, parameters:parameters, encodingType: HttpBodyEncoding,ShowProgress:true) { (isSuccess, dictData) in
            if isSuccess == true {
               
               
                
                
            }else{
                self.showAlertMessage(message:dictData["message"] as! String)
            }
        }
        
        
   
        
        
        
    }

   
}
extension TVShowDetailVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
         return self.arr_episodeList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let dict = self.arr_episodeList[indexPath.row] as! NSDictionary
        
         let img_logo = cell.contentView.viewWithTag(10) as? UIImageView
         let lbl_episodeName = cell.contentView.viewWithTag(11) as? UILabel
         let lbl_episodeTime = cell.contentView.viewWithTag(12) as? UILabel
         let lbl_episodeDes = cell.contentView.viewWithTag(13) as? UILabel
        
        let string = NSString(string:(dict .value(forKey: "duration") as? String)!)
        
      
        lbl_episodeName?.text = dict .value(forKey: "episode_name") as? String
        lbl_episodeTime?.text =  string.doubleValue.asString(style: .short)
        lbl_episodeDes?.text = dict .value(forKey: "description") as? String
        img_logo?.image = (dict.value(forKey: "episode_video") as! String).createThumbnailOfVideoFromRemoteUrl()
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
          let dict = self.arr_episodeList[indexPath.row] as! NSDictionary
        
        let strVideo = dict .value(forKey: "episode_video") as! String
        let obj = WMPlayerModel()
        obj.title = "";
        obj.videoURL = URL(string: strVideo)
        
        let extns =  obj.videoURL.pathExtension
        if (extns.count > 0) {
            let objDetailPlayer = DetailViewController()
            objDetailPlayer.playerModel = obj
            self.navigationController?.pushViewController(objDetailPlayer, animated: true)
        }else {
            let objDetailPlayer = WNPlayerDetailViewController()
            objDetailPlayer.playerModel = obj;
            self.navigationController?.pushViewController(objDetailPlayer, animated: true)
            
      }
    }
        
}
