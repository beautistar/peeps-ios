//
//  GroupVC.swift
//  Peeps
//
//  Created by Mac on 23/12/18.
//  Copyright © 2018 Kirti. All rights reserved.
//

import UIKit
import GoogleMobileAds

class GroupVC: UIViewController {

    
   
     var arr_channelsAll: [Channel] = []
      var arr_channelsGroup: [Channel] = []
     var arr_channelsGroupOriginal: [Channel] = []
     @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var tbl_group: UITableView!
    @IBOutlet weak var search_group: UISearchBar!
     @IBOutlet weak var imgvw_back: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
            let channelGroup = arr_channelsAll.unique{$0.tvgroup}
          arr_channelsGroup = channelGroup.sorted {$0.tvgroup < $1.tvgroup}
        let channelFirst = Channel.init(title: "", tvLogo: "", tvgroup:"All Channel groups", strurl: "")
         arr_channelsGroup .insert(channelFirst, at: 0)
        arr_channelsGroupOriginal = arr_channelsGroup;
        
        self.navigationItem.title = "Groups"
        
        bannerView.adUnitID = kBannerid
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        
          self.imgvw_back.image = UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!)
        
        self.setBackbutton()
        self.tbl_group.tableFooterView = UIView.init();
        
        self.search_group.layer.borderWidth = 1;
        self.search_group.layer.borderColor = UIColor.white.cgColor
        self.search_group.layer.masksToBounds = true;
        
        // Do any additional setup after loading the view.
    }
    

   

}
extension GroupVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arr_channelsGroup.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let lblName = cell.contentView.viewWithTag(10) as? UILabel
         let lblgroupCount = cell.contentView.viewWithTag(11) as? UILabel
        lblgroupCount?.backgroundColor = UIColor.red
        lblgroupCount?.layer.cornerRadius = 8;
        lblgroupCount?.layer.masksToBounds = true;
        lblgroupCount?.textColor = UIColor.white;
        
        let channel = self.arr_channelsGroup[indexPath.row]
         lblName?.text = channel.tvgroup
        
         let filteredArray = arr_channelsAll.filter { $0.tvgroup == channel.tvgroup }
        
        if indexPath.row == 0{
          lblgroupCount?.text = String(arr_channelsAll.count)
        }else
        {
            lblgroupCount?.text = String(filteredArray.count)
        }
      
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view .endEditing(true)
           let channel = self.arr_channelsGroup[indexPath.row]
        
        let filteredArray = arr_channelsAll.filter { $0.tvgroup == channel.tvgroup }
        let objchannel = self.storyboard?.instantiateViewController(withIdentifier: "ChannelVC") as! ChannelVC
        objchannel.isFromFav = false;
        if indexPath.row == 0{
            objchannel.arr_channelsList = arr_channelsAll
        }else
        {
            objchannel.arr_channelsList = filteredArray
        }
        
        objchannel.str_title =  channel.tvgroup
        self.navigationController?.pushViewController(objchannel, animated: false)
    }
}
extension GroupVC : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if searchText.count == 0 {
            arr_channelsGroup = arr_channelsGroupOriginal
        }else {
            arr_channelsGroup = self.arr_channelsGroupOriginal.filter {$0.tvgroup.lowercased().contains(searchText.lowercased()) }
        }
        tbl_group .reloadData()
     
        
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view .endEditing(true)
    }
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//         arr_channelsGroup = arr_channelsGroupOriginal
//        self.view.endEditing(true)
//          tbl_group .reloadData()
//    }
}
extension GroupVC :GADBannerViewDelegate
{
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
