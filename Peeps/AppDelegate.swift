//
//  AppDelegate.swift
//  Peeps
//
//  Created by Mac on 22/12/18.
//  Copyright © 2018 Kirti. All rights reserved.
//

import UIKit
import GoogleMobileAds
import FAPanels
//import Localize
import Fabric
import Crashlytics

import Localize_Swift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
       
        
        //let localize = Localize.shared
        print(Localize.availableLanguages())
        print(Localize.currentLanguage())
        
          print("khome".localized())
          print( "hello.world".localized() )
        
        if (UserDefaults.standard.string(forKey: kSelectedImage) == nil) {
            UserDefaults.standard .setValue("0_theme", forKey: kSelectedImage)
        }
        
        if (UserDefaults.standard.string(forKey: kSelectedLanguage) == nil) {
            UserDefaults.standard .setValue("en", forKey: kSelectedLanguage)
        }
        
        
        Localize .setCurrentLanguage(UserDefaults.standard.string(forKey: kSelectedLanguage)!)
        UserDefaults.standard .synchronize()
        
       // kSelectedImage
        if #available(iOS 10.0, *) {
            UINavigationBar.appearance().barTintColor = UIColor(hexString: UserDefaults.standard.string(forKey: kSelectedImage)!)
        } else {
            // Fallback on earlier versions
        }
       
       
        UINavigationBar .appearance().setBackgroundImage(UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!), for:.default)
        
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        
        Fabric.with([Crashlytics.self])

        
       
        
        GADMobileAds.configure(withApplicationID: "ca-app-pub-3940256099942544~1458002511")
       
        if UserDefaults.standard.bool(forKey: klogged) {
            self.gotoSidemenu()
        }
        
        return true
    }
     func gotoSidemenu()
     {
        UIView .setAnimationsEnabled(false)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let leftMenuVC: SideMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        
        let centerVC: HomeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let centerNavVC = UINavigationController(rootViewController: centerVC)
        
            let rootController = FAPanelController()
            window?.rootViewController = rootController
            rootController.leftPanelPosition = .front
           rootController.center(centerNavVC).left(leftMenuVC)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1 ) {
             UIView .setAnimationsEnabled(false)
        }
        
        
     }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//
//        return .allButUpsideDown
//    }


}

