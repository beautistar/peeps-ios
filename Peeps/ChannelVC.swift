//
//  ChannelVC.swift
//  Peeps
//
//  Created by Mac on 23/12/18.
//  Copyright © 2018 Kirti. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMobileAds
import Alamofire
import EmptyDataSet_Swift
class ChannelVC: UIViewController {
    @IBOutlet weak var consHeightSearchBar: NSLayoutConstraint!
    
   // @IBOutlet weak var tbl_channel: UITableView!
      @IBOutlet weak var collection_channel: UICollectionView!
    @IBOutlet weak var searchbar_channel: UISearchBar!
         @IBOutlet weak var bannerView: GADBannerView!
     @IBOutlet weak var imgvw_back: UIImageView!
      var arr_videoList = NSMutableArray()
     var arr_channelsList: [Channel] = []
      var arr_channeFavList: [Channel] = []
     var arr_channelsListOriginal: [Channel] = []
     var str_title = String()
     var isFromFav = Bool()
     var isFromRemoteList = Bool()
     var strPlayListID = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imgvw_back.image = UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!)
        
        arr_channelsListOriginal = arr_channelsList;
        self.navigationItem.title = str_title;
        arr_channeFavList  = self.getFavoritedata() as! [Channel]
           self.navigationItem.title = "Channels"
        if self.isFromFav {
            setSideMenubtn()
             self.navigationItem.title = "Favorite"
        }
        else if self.isFromRemoteList {
             self.setBackbutton()
            self .getRemoteVideoList()
             self.navigationItem.title = str_title
            self.searchbar_channel.isHidden = true;
            self.consHeightSearchBar.constant = 0;
           // self.tbl_channel.tableHeaderView = nil;
        }
        else {
              self.setBackbutton()
            self.navigationItem.title = str_title;
        }
        self.collection_channel.emptyDataSetSource = self;
//        playlist_id
//        
//            {
//                "video_list": [],
//                "message": "Success.",
//                "result_code": 200
//        }
        
        bannerView.adUnitID = kBannerid
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
       
       
        self.searchbar_channel.layer.borderWidth = 1;
        self.searchbar_channel.layer.borderColor = UIColor.white.cgColor
        self.searchbar_channel.layer.masksToBounds = true;
      
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
    }
    func getRemoteVideoList()
    {
        let parameters: Parameters = [
            "playlist_id":strPlayListID
        ]
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification?.show(true)
        Alamofire.request(kHostpath+kgetVideolist, method: .post, parameters: parameters, encoding: URLEncoding(destination: .methodDependent)).responseJSON { (response) in
            if response.result.isSuccess {
                let jsonDic = response.result.value as! NSDictionary
                print(jsonDic)
                
                if jsonDic["result_code"] as! Int == 200 {
                    self.arr_videoList = NSMutableArray(array: jsonDic .object(forKey: "video_list") as! NSArray)
                    
                }else{
                    self.showAlertMessage(message:jsonDic["message"] as! String)
                }
                
            }
            loadingNotification?.hide(true)
            
            self.collection_channel .reloadData()
        }
    }
    @objc func btnActionFavClicked(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        
        
        let buttonPosition = sender.convert(CGPoint.zero, to:self.collection_channel)
        let indexPathGlobal = self.collection_channel.indexPathForItem(at: buttonPosition)!
        
        let objmarklistdata:Channel = self.arr_channelsList[indexPathGlobal.row] as Channel
        
       
        
        if arr_channeFavList.contains(where: { $0.title == objmarklistdata.title }) {
          arr_channeFavList = arr_channeFavList.filter { $0.title != objmarklistdata.title }
            
        } else {
            arr_channeFavList .append(objmarklistdata)
        }
        
        self.saveFavoritedata(arr: arr_channeFavList)
        
        if self.isFromFav {
            arr_channelsList .remove(at: indexPathGlobal.row)
            self.collection_channel .reloadData()
        }
        
       
    
    }

}
extension ChannelVC : UICollectionViewDelegateFlowLayout{
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width - 45
        return CGSize(width: collectionViewWidth/2, height: collectionViewWidth/2)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 20
//    }
}
extension ChannelVC : UICollectionViewDataSource,UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.isFromRemoteList{
            return self.arr_videoList.count
        }
        return self.arr_channelsList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! UICollectionViewCell
        
        let imglogo = cell.contentView.viewWithTag(9) as? UIImageView
        let lblName = cell.contentView.viewWithTag(10) as? UILabel
       // let lblurl = cell.contentView.viewWithTag(11) as? UILabel
        
        let btnfav = cell.contentView.viewWithTag(20) as? UIButton
        
        if self.isFromRemoteList {
            btnfav?.isHidden = true
            let obj = self.arr_videoList[indexPath.row] as! NSDictionary
            lblName?.text = obj .value(forKey: "video_title") as? String
          //  lblurl?.text = obj .value(forKey: "video_upload") as? String
            
            
            
        }else {
            btnfav?.addTarget(self, action: #selector(btnActionFavClicked(_:)), for: .touchUpInside)
            let channel = self.arr_channelsList[indexPath.row]
            lblName?.text = channel.title
 //           lblurl?.text = channel.strurl
//            imglogo?.sd_setImage(with: URL(string: channel.tvLogo), completed: { (image, error, sdimageCache, url) in
//            })
            
            imglogo?.sd_setImage(with: URL(string: channel.tvLogo), placeholderImage: UIImage(named: "logo"))
            
            if arr_channeFavList.contains(where: { $0.title == channel.title }) {
                btnfav?.isSelected = true;
            } else {
                btnfav?.isSelected = false
            }
        }
        
        
        return cell

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        let obj = WMPlayerModel()
        obj.title = "";
        
        if self.isFromRemoteList {
            
            let dict = self.arr_videoList[indexPath.row] as! NSDictionary
            if (dict .value(forKey: "file_type") as! String ).lowercased() == "file"{
                obj.videoURL = URL(string: (dict .value(forKey: "video_upload") as? String)!)
            }else {
                
                MBProgressHUD .showAdded(to: self.view, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now()+0.01) {
                    do{
                        let data =  try Data(contentsOf: URL(string: (dict .value(forKey: "video_upload") as? String)!)!)
                        if  let string = String(data: data, encoding: .utf8) {
                            let channelarr = self .extractChannelsFromRawString(string)
                            
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if channelarr .count > 0{
                                let objgroup = self.storyboard?.instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
                                objgroup.arr_channelsAll = channelarr;
                                self.navigationController?.pushViewController(objgroup, animated: false)
                            }
                            else
                            {
                                self .showAlertMessage(message: "Channel parsing error")
                            }
                        }
                    }catch{
                        MBProgressHUD.hide(for:self.view, animated: true)
                        print("copy failure.")
                    }
                }
                
                return;
            }
            //   http://web24.live:8080/get.php?username=2493496367&password=xrhJ1Dgy9U&type=m3u_plus&output=ts
            
        }else{
            
            let channel = self.arr_channelsList[indexPath.row]
            obj.videoURL = URL(string:channel.strurl)
            print(channel.strurl)
        }
        
        
        print(obj.videoURL)
        
        
        let extns =  obj.videoURL.pathExtension
        if (extns.count > 0) {
            let objDetailPlayer = DetailViewController()
            objDetailPlayer.playerModel = obj
            self.navigationController?.pushViewController(objDetailPlayer, animated: true)
        }else {
            let objDetailPlayer = WNPlayerDetailViewController()
            objDetailPlayer.playerModel = obj;
            self.navigationController?.pushViewController(objDetailPlayer, animated: true)
        }
        
       
    }
    
}
extension ChannelVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.isFromRemoteList{
             return self.arr_videoList.count
        }
        return self.arr_channelsList.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let imglogo = cell.contentView.viewWithTag(9) as? UIImageView
        let lblName = cell.contentView.viewWithTag(10) as? UILabel
        let lblurl = cell.contentView.viewWithTag(11) as? UILabel
        
        let btnfav = cell.contentView.viewWithTag(20) as? UIButton
        
        if self.isFromRemoteList {
            btnfav?.isHidden = true
             let obj = self.arr_videoList[indexPath.row] as! NSDictionary
            lblName?.text = obj .value(forKey: "video_title") as? String
            lblurl?.text = obj .value(forKey: "video_upload") as? String
            
            
            
        }else {
            btnfav?.addTarget(self, action: #selector(btnActionFavClicked(_:)), for: .touchUpInside)
            let channel = self.arr_channelsList[indexPath.row]
            lblName?.text = channel.title
            lblurl?.text = channel.strurl
            imglogo?.sd_setImage(with: URL(string: channel.tvLogo), completed: { (image, error, sdimageCache, url) in
            })
            if arr_channeFavList.contains(where: { $0.title == channel.title }) {
                btnfav?.isSelected = true;
            } else {
                 btnfav?.isSelected = false
            }
        }
        
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
      

        
        let obj = WMPlayerModel()
        obj.title = "";
      
        if self.isFromRemoteList {
            
               let dict = self.arr_videoList[indexPath.row] as! NSDictionary
            if (dict .value(forKey: "file_type") as! String ).lowercased() == "file"{
                obj.videoURL = URL(string: (dict .value(forKey: "video_upload") as? String)!)
            }else {
                
                MBProgressHUD .showAdded(to: self.view, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now()+0.01) {
                    do{
                        let data =  try Data(contentsOf: URL(string: (dict .value(forKey: "video_upload") as? String)!)!)
                        if  let string = String(data: data, encoding: .utf8) {
                            let channelarr = self .extractChannelsFromRawString(string)
                            
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if channelarr .count > 0{
                                let objgroup = self.storyboard?.instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
                                objgroup.arr_channelsAll = channelarr;
                                self.navigationController?.pushViewController(objgroup, animated: false)
                            }
                            else
                            {
                                self .showAlertMessage(message: "Channel parsing error")
                            }
                        }
                    }catch{
                        MBProgressHUD.hide(for:self.view, animated: true)
                        print("copy failure.")
                    }
                }
              
                return;
            }
         //   http://web24.live:8080/get.php?username=2493496367&password=xrhJ1Dgy9U&type=m3u_plus&output=ts
            
        }else{
            
             let channel = self.arr_channelsList[indexPath.row]
             obj.videoURL = URL(string:channel.strurl)
              print(channel.strurl)
        }
        
       
 // 
        
        let extns =  obj.videoURL.pathExtension
        if (extns.count > 0) {
            let objDetailPlayer = DetailViewController()
            objDetailPlayer.playerModel = obj
            self.navigationController?.pushViewController(objDetailPlayer, animated: true)
        }else {
            let objDetailPlayer = WNPlayerDetailViewController()
            objDetailPlayer.playerModel = obj;
            self.navigationController?.pushViewController(objDetailPlayer, animated: true)
        }
        
      
        
//        wnDetailVC.p = playerModel;
//        [self.navigationController pushViewController:wnDetailVC animated:YES];
        

      
    }
    
}
extension ChannelVC : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if searchText.count == 0 {
            arr_channelsList = arr_channelsListOriginal
        }else {
            arr_channelsList = self.arr_channelsListOriginal.filter {$0.title.lowercased().contains(searchText.lowercased()) }
        }
        self.collection_channel .reloadData()
        
        
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view .endEditing(true)
    }
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//         arr_channelsList = arr_channelsListOriginal
//         self.view.endEditing(true)
//         self.tbl_channel .reloadData()
//    }
}
extension ChannelVC : EmptyDataSetSource,EmptyDataSetDelegate
{
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        var str = ""
        
        //  if self.isfromRemote {
        
        if self.isFromFav {
            str = "No favorite channel added"
        }
         
        let attrs = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont .systemFont(ofSize: 20)]
        return NSAttributedString(string: str, attributes: attrs)
    }
}
extension ChannelVC :GADBannerViewDelegate
{
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
