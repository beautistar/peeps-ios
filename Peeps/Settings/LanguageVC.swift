//
//  LanguageVC.swift
//  Peeps
//
//  Created by Mac on 1/6/19.
//  Copyright © 2019 Kirti. All rights reserved.
//

import UIKit
import Localize_Swift


class LanguageVC: UIViewController {

      @IBOutlet weak var tbl_language: UITableView!
       @IBOutlet weak var imgvw_back: UIImageView!
    
    var arr_language : [[String:String]] = [[String:String]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Select Language"
        self.tbl_language.tableFooterView = UIView.init();
      
         self.imgvw_back.image = UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!)
        
         arr_language .append(["name":"Arabic","code":"ar"])
        arr_language .append(["name":"English","code":"en"])
        arr_language .append(["name":"Turkish","code":"tr"])
        arr_language .append(["name":"Urdu","code":"ur"])
            arr_language .append(["name":"Philippines","code":"fil"])
        arr_language .append(["name":"Spanish","code":"es"])
        
         self.setBackbutton()

        
//        1. Arabic
//        2. English
//        3. Turkish
//        4. Urdu (Indian - Pakistani )
//        5. Philippines
//        6. Spanish
        
  
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LanguageVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arr_language.count;
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        //
//        let lblName = cell.contentView.viewWithTag(10) as? UILabel
//        lblName?.text = self.arr_language[indexPath.row]
       cell.textLabel?.text = self.arr_language[indexPath.row]["name"]
          cell.accessoryType = .none
        if UserDefaults.standard.string(forKey: kSelectedLanguage) == self.arr_language[indexPath.row]["code"]!
        {
            cell.accessoryType = .checkmark
        }
        
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        Localize .setCurrentLanguage(self.arr_language[indexPath.row]["code"]!)
         UserDefaults.standard .setValue(self.arr_language[indexPath.row]["code"]!, forKey: kSelectedLanguage)
        UserDefaults.standard .synchronize()
        self.tbl_language .reloadData()
        
        
    }
}
