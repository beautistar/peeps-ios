//
//  ThemeViewVC.swift
//  Peeps
//
//  Created by Mac on 1/6/19.
//  Copyright © 2019 Kirti. All rights reserved.
//

import UIKit

class ThemeViewVC: UIViewController {

    @IBOutlet weak var btn_theme: UIButton!
    
    
    @IBOutlet weak var imgvw_back: UIImageView!
    var strcolor = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btn_theme.layer.cornerRadius =  10
        self.btn_theme.layer.borderWidth = 1
        self.btn_theme.layer.borderColor  = UIColor.white.cgColor;
        
        self.navigationController?.isNavigationBarHidden = true;
        self.imgvw_back.image = UIImage(named: self.strcolor)
      //  self.view.backgroundColor = UIColor(hexString: self.strcolor)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnActinonThemeVC(_ sender: UIButton) {
        
        UserDefaults.standard .setValue(self.strcolor, forKey: kSelectedImage)
        UserDefaults.standard .synchronize()
        
//          UINavigationBar.appearance().barTintColor = UIColor(hexString: UserDefaults.standard.string(forKey: kSelectedImage)!)
//          self.navigationController?.navigationBar.barTintColor = UIColor(hexString: UserDefaults.standard.string(forKey: kSelectedImage)!)
        
        
        UINavigationBar .appearance().setBackgroundImage(UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!), for:.default)
         self.navigationController?.isNavigationBarHidden = false;
        self.navigationController?.popToRootViewController(animated: false)
//        let centerVC: HomeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//        let centerNavVC = UINavigationController(rootViewController: centerVC)
//        panel?.center(centerNavVC)
        
    }
    @IBAction func btnActionBackClicked(_ sender: UIButton) {
        
         self.navigationController?.isNavigationBarHidden = false;
        self.navigationController?.popViewController(animated: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
