//
//  SettingsVC.swift
//  Peeps
//
//  Created by Mac on 1/4/19.
//  Copyright © 2019 Kirti. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

     @IBOutlet weak var tbl_settings: UITableView!
     @IBOutlet weak var imgvw_back: UIImageView!
     var arr_setting: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Settings"
        self.tbl_settings.tableFooterView = UIView.init();
         setSideMenubtn()
        
           arr_setting .append("kChangeTheme")
           arr_setting .append("kChangeLanguage")
        
       

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
           self.imgvw_back.image = UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!)
         // self.view.backgroundColor = UIColor(hexString: UserDefaults.standard.string(forKey: kSelectedImage)!)
             self.tbl_settings .reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SettingsVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2;
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//
       let lblName = cell.contentView.viewWithTag(10) as? UILabel
        lblName?.text = self.arr_setting[indexPath.row].localized()  //self.arr_folderList[indexPath.row].lastPathComponent
//
//        let btnedit = cell.contentView.viewWithTag(20) as? UIButton
//        let btndelete = cell.contentView.viewWithTag(21) as? UIButton
//        btnedit?.addTarget(self, action: #selector(btnActionEditClicked(_:)), for: .touchUpInside)
//
//        btndelete?.addTarget(self, action: #selector(btnActionDeleteClicked(_:)), for: .touchUpInside)
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            let objThemeVC = self.storyboard?.instantiateViewController(withIdentifier: "ThemeVC") as! ThemeVC
            self.navigationController?.pushViewController(objThemeVC, animated: false)
            
        }else
        {
            let objLanguageVC = self.storyboard?.instantiateViewController(withIdentifier: "LanguageVC") as! LanguageVC
            self.navigationController?.pushViewController(objLanguageVC, animated: false)
        }
       
        
        
        
    }
}
