//
//  ThemeVC.swift
//  Peeps
//
//  Created by Mac on 1/6/19.
//  Copyright © 2019 Kirti. All rights reserved.
//

import UIKit

class ThemeVC: UIViewController {
    
      @IBOutlet weak var clc_theme: UICollectionView!
     @IBOutlet weak var imgvw_back: UIImageView!
      var items = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.setBackbutton()
        
        var i = 0
        while i <= 16 {
            print(i)
            items .append(String(i)+"_theme")
            i = i + 1
        }

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
       
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ThemeVC :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath)
        
        let imgvw = cell.contentView .viewWithTag(10) as! UIImageView
        imgvw.image = UIImage(named: self.items[indexPath.row])
        

       // cell.backgroundColor = UIColor(hexString: self.items[indexPath.row])
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
//        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        
        let width = (UIScreen.main.bounds.size.width - 60 )/3
        
        return CGSize(width: width, height: width)
    }
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        let objThemeViewVC = self.storyboard?.instantiateViewController(withIdentifier: "ThemeViewVC") as! ThemeViewVC
        objThemeViewVC.strcolor = self.items[indexPath.item]
        self.navigationController?.pushViewController(objThemeViewVC, animated: false)
    }
}
