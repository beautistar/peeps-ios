//
//  customTblCell.swift
//  Peeps
//
//  Created by kirti on 01/05/19.
//  Copyright © 2019 Kirti. All rights reserved.
//

import UIKit

class customTblCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    var arr_collectionList = NSMutableArray()
    var selectedIndex = Int()
    var currentSection = Int()
    var objViewController : HomeVC?
//     @IBOutlet weak var btnPlay: UIButton!
//     @IBOutlet weak var pagecontrol: UIPageControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        if selectedIndex == 0 {
//            return (self.arr_collectionList[section] as AnyObject).count
//        }else if selectedIndex == 1 {
//            return (self.arr_collectionList[section] as AnyObject).count
//        }
        
        if self.arr_collectionList.count > 0{
            print(self.arr_collectionList.count)
        return self.arr_collectionList.count
        }else {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let imgvw_banner = cell.contentView.viewWithTag(10) as! UIImageView
        let lbl_name = cell.contentView.viewWithTag(11) as! UILabel
        let btn_play = cell.contentView.viewWithTag(20) as! UIButton
        let pagecontrol = cell.contentView.viewWithTag(21) as! UIPageControl
        btn_play.isUserInteractionEnabled = false
        
        var dict = NSDictionary()
        if selectedIndex == 0 {
          //  let array = self.arr_collectionList[indexPath.section] as! NSArray
            dict = self.arr_collectionList[indexPath.row] as! NSDictionary
            lbl_name.text = dict .value(forKey: "series_name") as? String
            btn_play.isHidden = true
            
        }else if selectedIndex == 1 {
//            let array = self.arr_collectionList[indexPath.section] as! NSArray
           // dict = array[indexPath.row] as! NSDictionary
              dict = self.arr_collectionList[indexPath.row] as! NSDictionary
            lbl_name.text = dict .value(forKey: "name") as? String
            btn_play.isHidden = false
        }
        if currentSection == 0 {
            pagecontrol.isHidden = false
            pagecontrol.numberOfPages = self.arr_collectionList.count
        }else{
             pagecontrol.isHidden = true
        }
        
        if let coverImage = dict .value(forKey: "cover_image")as? String {
            imgvw_banner.sd_setImage(with: URL(string:coverImage), completed: { (image, error, sdimageCache, url) in
            })
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedIndex == 0{
          ///  let array = self.arr_collectionList[indexPath.section] as! NSArray
            let dict =  self.arr_collectionList[indexPath.row] as! NSDictionary
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objtvdetail = storyBoard.instantiateViewController(withIdentifier: "TVShowDetailVC") as! TVShowDetailVC
            objtvdetail.dict_detail = dict
            self.objViewController?.navigationController?.pushViewController(objtvdetail, animated: false)
        }else{
         //   let array = self.arr_collectionList[indexPath.section] as! NSArray
             let dict =  self.arr_collectionList[indexPath.row] as! NSDictionary
            
            let strVideo = dict .value(forKey: "movie_video") as! String
            let obj = WMPlayerModel()
            obj.title = "";
            obj.videoURL = URL(string: strVideo)
            let extns =  obj.videoURL.pathExtension
            if (extns.count > 0) {
                let objDetailPlayer = DetailViewController()
                objDetailPlayer.playerModel = obj
                self.objViewController?.navigationController?.pushViewController(objDetailPlayer, animated: true)
            }else {
                let objDetailPlayer = WNPlayerDetailViewController()
                objDetailPlayer.playerModel = obj;
                self.objViewController?.navigationController?.pushViewController(objDetailPlayer, animated: true)
              }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var dict = NSDictionary()
        if selectedIndex == 0 {
          //  let array = self.arr_collectionList[indexPath.section] as! NSArray
            dict = self.arr_collectionList[indexPath.row] as! NSDictionary
            
        }else if selectedIndex == 1 {
          //  let array = self.arr_collectionList[indexPath.section] as! NSArray
              dict = self.arr_collectionList[indexPath.row] as! NSDictionary
        }
        
        
        if let imagesize = dict .value(forKey: "image_appearance") as? String {
            if imagesize.lowercased() == "small"{
                let cellSize = CGSize(width: (collectionView.bounds.width - (2 * 10))/3, height: 130)
                return cellSize
            }
            else if imagesize.lowercased() == "medium"{
                let cellSize = CGSize(width: (collectionView.bounds.width - (2 * 10))/2, height: 130)
                return cellSize
            }else {
                let cellSize = CGSize(width:collectionView.bounds.width , height: 300)
                return cellSize
            }
            
        }
        return CGSize(width: 0, height: 0)
        
    }
//    func collectionView(_ collectionView: UICollectionView,viewForSupplementaryElementOfKind kind: String,
//                        at indexPath: IndexPath) -> UICollectionReusableView {
//        
//        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "clcheader", for: indexPath)
//        switch kind {
//            
//        case UICollectionView.elementKindSectionHeader:
//            
//            var dict = NSDictionary()
//            if selectedIndex == 0 {
//                if let array = self.arr_collectionList[indexPath.section] as? NSArray,array.count > 0 {
//                    dict = array[0] as! NSDictionary
//                }
//            }
//            else if selectedIndex == 1 {
//                if let array = self.arr_collectionList[indexPath.section] as? NSArray,array.count > 0 {
//                    dict = array[0] as! NSDictionary
//                }
//            }
//            
//            
//            let lbl = headerView .viewWithTag(10) as? UILabel
//            lbl?.text = dict .value(forKey: "category_name")as? String
//            headerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
//            return headerView
//            
//        default:
//            assert(false, "Unexpected element kind")
//        }
//        return headerView
//    }

}
