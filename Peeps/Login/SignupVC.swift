//
//  SignupVC.swift
//  Peeps
//
//  Created by Mac on 1/26/19.
//  Copyright © 2019 Kirti. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD

let kUserID = "kUserID"
let kUserName = "kUserName"
let kUserProfileUrl = "kUserProfileUrl"

class SignupVC: UIViewController {

    @IBOutlet weak var text_firstName: UITextField!
    @IBOutlet weak var text_lastName: UITextField!
    @IBOutlet weak var text_password: UITextField!
    @IBOutlet weak var text_Confirmpassword: UITextField!
    @IBOutlet weak var text_email: UITextField!
    @IBOutlet weak var btn_signup: UIButton!
     @IBOutlet weak var btn_profile: UIButton!
     @IBOutlet weak var imgvw_back: UIImageView!
     @IBOutlet weak var imageview_profile: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
         self.imgvw_back.image = UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!)
        btn_signup.setBorderAndCorner()
        self.navigationItem.title = "Register"
        self.setBackbutton()
        self.imageview_profile.layer.cornerRadius = self.imageview_profile.frame.width/2;
        self.imageview_profile.layer.masksToBounds = true;
        //self.btn_profile.imageView?.contentMode =
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnActionProfileClicked(_ sender: Any) {
        
        let alert = UIAlertController(title:nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnActionRegisterClicked(_ sender: UIButton) {
        //first_name,last_name,email,password,photo
        
       // profile
         if self.text_firstName.text?.isContainText() == false {
            self .showAlertMessage(message:"Enter first name")
         }
         else if self.text_lastName.text?.isContainText() == false {
            self .showAlertMessage(message:"Enter last name")
         }
        else if self.text_email.text?.isValidEmail() == false {
            self .showAlertMessage(message:"Enter valid eamil")
        }
         else if  self.text_password.text?.isContainText() == false {
            self .showAlertMessage(message:"Enter password ")
         }
         else if  self.text_Confirmpassword.text?.isContainText() == false {
            self .showAlertMessage(message:"Enter confirm password")
         }
         else if  self.text_password.text != self.text_Confirmpassword.text {
            self .showAlertMessage(message:"Password does not match !!!")
         }
         else if  imageview_profile.image ==  UIImage(named: "profile") {
            self .showAlertMessage(message:"Please select profile picture")
         }
         
        else {
            
            let parameters: Parameters = [
                "first_name": text_firstName.text!,
                "last_name": text_lastName.text!,
                "email": text_email.text!,
                "password": text_password.text!,
            
            ]
            
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification?.show(true)
            
            let imgData = imageview_profile.image?.jpegData(compressionQuality: 0.1)
            
            
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData!, withName: "photo",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                } //Optional for extra parameters
            },
            to:kHostpath + kRegister)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        print(response.result.value)
                         loadingNotification?.hide(true)
                        if let dict = response.result.value as? NSDictionary
                        {
                            if (dict["result_code"] as! Int) == 200 {
                                UserDefaults.standard .set(dict["id"], forKey: kUserID)
                                UserDefaults.standard .synchronize()
                                
                                UserDefaults.standard .set(self.text_firstName.text! + " " + self.text_lastName.text!, forKey: kUserName)
                                UserDefaults.standard .set(dict["image_url"]!, forKey: kUserProfileUrl)
                                
                                UserDefaults.standard .set(true, forKey: klogged)
                                UserDefaults.standard .synchronize()
                                
                                let objAppdelegate = UIApplication.shared.delegate as! AppDelegate
                                objAppdelegate .gotoSidemenu()
                            }else{
                                if let message = dict["message"] as? String {
                                   self.showAlertMessage(message:message)
                                }
                            }
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    loadingNotification?.hide(true)
                    self.showAlertMessage(message:encodingError.localizedDescription)
                    
                }
            }
            
            
        }
        
    }
  
    
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have perission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
   

    

}
extension SignupVC :UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    //MARK:-- ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Error: \(info)")
            return;
        }
        
        self.imageview_profile.image = selectedImage;
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         dismiss(animated: true, completion: nil)
    }
}
extension SignupVC :UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view .endEditing(true)
        return true
    }
}
