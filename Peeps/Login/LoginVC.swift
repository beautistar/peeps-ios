//
//  LoginVC.swift
//  Peeps
//
//  Created by Mac on 1/26/19.
//  Copyright © 2019 Kirti. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
class LoginVC: UIViewController {

   
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var text_email: UITextField!
    @IBOutlet weak var text_password: UITextField!
     @IBOutlet weak var imgvw_back: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
           self.imgvw_back.image = UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!)
        btn_login.setBorderAndCorner()
        
        self.navigationItem.title = "Login"
        // Do any additional setup after loading the view.
    }
    @IBAction func btnActionLoginClicked(_ sender: UIButton) {
       

        
        if text_email.text?.isValidEmail() == false {
          self .showAlertMessage(message:"Enter valid eamil")
        }else if (text_password.text?.isContainText() == false) {
          self .showAlertMessage(message:"Enter password")
        }
        else {

            let parameters = [
                "email": text_email.text!,
                "password": text_password.text!] as [String : AnyObject]
            WebserviceHelper.postWebServiceCall(urlString: kHostpath+kLogin, parameters:parameters, encodingType: HttpBodyEncoding,ShowProgress:true) { (isSuccess, dictData) in
                if isSuccess == true {
                    if let dict = dictData .object(forKey: "user_model") as? NSDictionary{
                    UserDefaults.standard .set(dict["user_id"]!, forKey: kUserID)
                    UserDefaults.standard .set((dict["first_name"] as! String) + " " + (dict["last_name"] as! String), forKey: kUserName)
                    UserDefaults.standard .set(dict["photo_url"]!, forKey: kUserProfileUrl)
                    
                    UserDefaults.standard .set(true, forKey: klogged)
                    UserDefaults.standard .synchronize()
                    
                    let objAppdelegate = UIApplication.shared.delegate as! AppDelegate
                    objAppdelegate .gotoSidemenu()
                    }
                }else{
                    self.showAlertMessage(message:dictData["message"] as! String)
                }
            }
            

        }
 
        
        
        
    }
    @IBAction func btnActionRegisterClicked(_ sender: Any) {
        
        
        let objsignup = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
         self.navigationController?.pushViewController(objsignup, animated: true)
        
    }

   

}
extension LoginVC :UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view .endEditing(true)
        return true
    }
}
