//
//  WebserviceHelper.swift
//  AutoHome
//
//  Created by kirti on 11/01/18.
//  Copyright © 2018 Deep Kiran. All rights reserved.
//

import UIKit
import Alamofire
//import NVActivityIndicatorView
import Reachability
import PKHUD

var DefaultEncoding = "URLEncoding.default"
var HttpBodyEncoding = "URLEncoding.httpBody"

typealias CompletionHandler = (_ success:Bool,_ reponsedata:NSDictionary) -> Void
typealias ResponseHandler = (_ success:Bool,_ data:NSData, _ error : String) -> Void
typealias ConnectivityHandler = (_ success:Bool) -> Void

class Connectivity {
  
    class func internetConnection(completionHandler: @escaping ConnectivityHandler){
        
        //var Status:Bool = false
       
        let url = NSURL(string: "http://google.com/")
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "HEAD"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 1.0
        let session = URLSession.shared
       
        session.dataTask(with: request as URLRequest as URLRequest, completionHandler: {(data, response, error) in
          
            if let httpResponse = response as? HTTPURLResponse {
             
                if httpResponse.statusCode == 200 {
                    completionHandler(true)
                }
            }
            completionHandler(false)

        }).resume()
        
    }
    
     class func connetivityAvailable() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }

}

class WebserviceHelper: NSObject {

    class func postWebServiceCall(urlString:String, parameters:[String:AnyObject], encodingType: String, ShowProgress:Bool, completionHandler: @escaping CompletionHandler){
    
    if Connectivity.connetivityAvailable() == false {
        print("internet is not available.")
         WebserviceHelper.showAlertMessage(message: "Please check your internet connection")
        completionHandler(false,NSMutableDictionary())
        return
    }
    
    if ShowProgress {
         HUD.show(.systemActivity)
      }
        var parametersnew = parameters
//        if let userid = UserDefaults.standard.value(forKey: KUserid) as? String {
//            parametersnew["user_id"] = userid as AnyObject;
//        }
   
        print(parametersnew)
    let encoding: ParameterEncoding!
    
    if encodingType == DefaultEncoding{
        encoding = JSONEncoding.default
    }else{
        encoding = URLEncoding.httpBody
    }
      
    Alamofire.request(urlString, method: HTTPMethod.post, parameters: parametersnew, encoding: encoding).responseJSON(completionHandler: { (response) in
      
      if  response.result.isSuccess {
     
        if ShowProgress {
            HUD.hide()
        }
        
        if let result = response.result.value{
            
           let JSONdict = result as! NSDictionary
             print(JSONdict)
            
            if JSONdict .value(forKey: "result_code") as! NSNumber == 200{
              
                completionHandler(true,JSONdict)
            
            }else if JSONdict .value(forKey: "code") as! NSNumber == 3001{
                
                completionHandler(false,JSONdict)
                
            }else{

                if JSONdict.count > 0 {
                    completionHandler(false,JSONdict)
                }else{
                    completionHandler(false,NSMutableDictionary())
                }

            }

          }
      }
     
        if response.result.isFailure{
       
            if ShowProgress{
                HUD.hide()
            }
            print("Lost Connection %@",urlString)
            
            if let responseCode = response.response?.statusCode as Int?{
                
              print("Lost Connection with code %@",response.result.value as Any)
              let responseDic = NSMutableDictionary()
               responseDic.setObject(responseCode, forKey: "statusCode" as NSCopying)
               completionHandler(false,NSMutableDictionary())
            }
            completionHandler(false,NSMutableDictionary())
       }
      
      
    })
    
  }
    class func showAlertMessage(message:String)
    {
        TinyToast.shared.show(message: message, valign: .center, duration: .normal)
    }
    
    class func showMessageOnTop(dicInfo : NSDictionary, view : UIView){
        
        if let message = dicInfo.object(forKey: "message") as? String {
            WebserviceHelper .showAlertMessage(message: message)
          //  AppDelegate.showMessageWithView(message: message, view: view)
        }
    }
  
    class func WebapiGetMethod(strurl:String, ShowProgress: Bool = false, completionHandler: @escaping CompletionHandler){
    
        if Connectivity.connetivityAvailable() == false {
           print("internet is not available.")
           completionHandler(false,NSMutableDictionary())
             WebserviceHelper.showAlertMessage(message: "Please check your internet connection")
          // AppDelegate .showMessage(message:"Please check your internet connection")
           return
        }
  
        if ShowProgress{
            HUD.show(.systemActivity)
        }
    
       Alamofire.request(strurl).responseJSON { response in
      
        if let status = response.response?.statusCode {
        switch(status){
        case 200:
         
            if let result = response.result.value{
                
                print(result)
                if let JSONdict = result as? NSMutableDictionary{
                    DispatchQueue.main.async {
                        completionHandler(true,JSONdict)
                    }
                }
                
                if ShowProgress {
                    HUD.hide()
                }
                
          }else{
            if ShowProgress {
                 HUD.hide()
            }
            completionHandler(false,NSMutableDictionary())
          }
          print("example success")
        
        default:
          print("error with get response status: \(status)")
          
            DispatchQueue.main.async {
                 if ShowProgress {
                  HUD.hide()
             }
          }
          
           completionHandler(false,NSMutableDictionary())
        }
      
        }
        
        DispatchQueue.main.async {
            if ShowProgress {
                HUD.hide()
            }
            
        }
      // completionHandler(false,NSMutableDictionary())
    }
  }
    
    
  class func WebapiGetBlasterMethod(strurl:String, ShowProgress: Bool = false, completionHandler: @escaping CompletionHandler){
        
        if Connectivity.connetivityAvailable() == false {
            print("internet is not available.")
            completionHandler(false,NSMutableDictionary())
             WebserviceHelper.showAlertMessage(message: "Please check your internet connection")
            return
        }
        
        if ShowProgress{
            HUD.show(.systemActivity)
        }
        
        Alamofire.request(strurl).responseJSON { response in
            
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                    
                    if let result = response.result.value{
                        
                        if ShowProgress {
                            HUD.hide()
                        }
                        
                        if let JSONdict = result as? NSMutableDictionary{
                            completionHandler(true,JSONdict)
                        }
                        
                    }else{
                        
                        if ShowProgress {
                            HUD.hide()
                        }
                        completionHandler(false,NSMutableDictionary())
                    }
                    print("example success")
                    
                default:
                    print("error with get response status: \(status)")
                    
                    DispatchQueue.main.async {
                        if ShowProgress {
                            HUD.hide()
                        }
                    }
                    
                    completionHandler(false,NSMutableDictionary())
                }
                
            }else{
                
                DispatchQueue.main.async {
                    if ShowProgress {
                        HUD.hide()
                    }
                }
                completionHandler(false,NSMutableDictionary())
            }

          }
      }
    
   }

