//
//  ViewController.swift
//  Peeps
//
//  Created by Mac on 22/12/18.
//  Copyright © 2018 Kirti. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import GoogleMobileAds
import EmptyDataSet_Swift

class HomeVC: UIViewController {


    @IBOutlet weak var segment_home: HMSegmentedControl!
    
    @IBOutlet weak var segment_cons_height: NSLayoutConstraint!
  //  @IBOutlet weak var clc_home: UICollectionView!
  
    
      var arr_tvShowList = NSMutableArray()
      var arr_movieList = NSMutableArray()
    
     var arr_folderList: [FolderList] = []
    var arr_RemotefolderList = NSMutableArray()
     var arr_OriginalRemotefolderList = NSMutableArray()
    @IBOutlet weak var tbl_home: UITableView!
    @IBOutlet weak var btn_add: UIButton!
      @IBOutlet weak var imgvw_back: UIImageView!
    var isfromFavorite = Bool()
     @IBOutlet weak var bannerView: GADBannerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self .loadData()
   
    
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.tbl_home.emptyDataSetSource = self;
        
        self.imgvw_back.image = UIImage(named: UserDefaults.standard.string(forKey: kSelectedImage)!)
        self.navigationItem.title = "Peeps"
        
        self.btn_add.layer.cornerRadius = self.btn_add.frame.width/2
        self.btn_add.layer.masksToBounds = true;
        
        self.tbl_home.tableFooterView = UIView.init()
        self.tbl_home.estimatedRowHeight = 60
        self.tbl_home.rowHeight = UITableView.automaticDimension
  
     
        bannerView.adUnitID = kBannerid
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        
        self.segment_home.sectionTitles = ["TV Shows","Movies","Remote Playlist"];
        self.segment_home.selectionIndicatorLocation = .down
        self.segment_home.selectionIndicatorColor = UIColor.white
        self.segment_home.backgroundColor = UIColor.clear;
        self.segment_home.selectionStyle = .fullWidthStripe
        self.segment_home.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont .systemFont(ofSize: 15)]
 
        self.fetchData()
        self.setSideMenubtn()
        if self.isfromFavorite
        {
            
        }else {
            self.getTVShowList()
            self.getMoviewList()
        }
       
       
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
   
    //MARK:Webservice Method
    func getTVShowList() {
        var parameters = [String : AnyObject]()
        var strurl = kHostpath+ktvShowList
        if self.isfromFavorite {
            parameters = ["user_id":UserDefaults.standard.value(forKey: kUserID)!] as [String : AnyObject]
            strurl = kHostpath+ktvgetFavorites
        }
        
        WebserviceHelper.postWebServiceCall(urlString:strurl, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
            
            if isSuccess == true{
                if let array = dictData .object(forKey: "tvshow_list") as? NSArray {
                      self.arr_tvShowList = self .filterArray(array: array)
                }
            }else{
                self.showAlertMessage(message:dictData["message"] as! String)
            }
            self.tbl_home .reloadData()
        }
    }
    func filterArray(array:NSArray) -> NSMutableArray {
        
        let arrayNew  = NSMutableArray()
        let array3 = array .value(forKey: "category_id") as! [String]
        
        
        var predicate = NSPredicate(format: "image_appearance = %@", "large")
       var array1  = array.filtered(using: predicate)
        if array1.count > 0{
            arrayNew .add(array1)
        }
        
        
        //let uniqueOrdered = Array(NSOrderedSet(array: array))
        let array2 = Array(NSOrderedSet(array: array3))
        for str in array2 {
             predicate = NSPredicate(format: "category_id = %@ AND image_appearance = %@", str as! CVarArg, "small")
             array1 = array.filtered(using: predicate)
            if array1.count > 0{
                arrayNew .add(array1)
            }
            predicate = NSPredicate(format: "category_id = %@ AND image_appearance = %@", str as! CVarArg, "medium")
            array1 = array.filtered(using: predicate)
            if array1.count > 0{
                arrayNew .add(array1)
            }
//             predicate = NSPredicate(format: "image_appearance = %@", str as! CVarArg, "large")
////            predicate = NSPredicate(format: "category_id = %@ AND image_appearance = %@", str as! CVarArg, "large")
//            array1 = array.filtered(using: predicate)
//            if array1.count > 0{
//                arrayNew .add(array1)
//            }
            
        }
        return arrayNew
    }
    func getMoviewList(){
        
        WebserviceHelper.postWebServiceCall(urlString: kHostpath+kmoviewList, parameters:[String : AnyObject](), encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
            if isSuccess == true {
                if let array = dictData .object(forKey: "movie_list") as? NSArray {
                    self.arr_movieList = self .filterArray(array: array)
                }
               
                
            }else{
                 self.showAlertMessage(message:dictData["message"] as! String)
            }
              self.tbl_home .reloadData()
        }
        
        

    }
    func loadData() {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as! String
        let path = documentDirectory.appending("Favorite.plist")
        let fileManager = FileManager.default
        if(!fileManager.fileExists(atPath: path)){
            if let bundlePath = Bundle.main.path(forResource: "Favorite", ofType: "plist"){
                let result = NSMutableDictionary(contentsOfFile: bundlePath)
                print("Bundle file myData.plist is -> \(result?.description)")
                do{
                    try fileManager.copyItem(atPath: bundlePath, toPath: path)
                }catch{
                    print("copy failure.")
                }
            }else{
                print("file myData.plist not found.")
            }
            
            
        }else{
            print("file myData.plist already exits at path.")
        }
        
        let resultDictionary = NSMutableDictionary(contentsOfFile: path)
        print("load myData.plist is ->\(resultDictionary?.description)")
        
        let myDict = NSDictionary(contentsOfFile: path)
        if let dict = myDict{
           // myItemValue = dict.object(forKey: myItemKey) as! String?
            //txtValue.text = myItemValue
        }else{
            print("load failure.")
        }
    }
    
    func saveData() {
        let propertylistSongs = arr_folderList.map{ $0.propertyListRepresentation }
         UserDefaults.standard.set(propertylistSongs, forKey: "folders")
    }
    
    func submitPlayListName(name:String)
    {
        let parameters = [
            "user_id": UserDefaults.standard .value(forKey: kUserID)!,
            "name":name
        ] as [String : AnyObject]
        
        WebserviceHelper.postWebServiceCall(urlString: kHostpath+kSubmitPlaylist, parameters:parameters, encodingType: HttpBodyEncoding,ShowProgress:true) { (isSuccess, dictData) in
            if isSuccess == true {
                self.fetchData()
            }else{
                self.showAlertMessage(message:dictData["message"] as! String)
            }
            self.tbl_home .reloadData()
        }
             
        
        /*
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification?.show(true)
        Alamofire.request(kHostpath+kSubmitPlaylist, method: .post, parameters: parameters, encoding: URLEncoding(destination: .methodDependent)).responseJSON { (response) in
            if response.result.isSuccess {
                let jsonDic = response.result.value as! NSDictionary
                print(jsonDic)
                
                if jsonDic["result_code"] as! Int == 200 {
                   self.fetchData()
                    
                }else{
                    self.showAlertMessage(message:jsonDic["message"] as! String)
                }
                
            }
            loadingNotification?.hide(true)
            
            self.tbl_home .reloadData()
        }
 */
    }
 func fetchData() {

    arr_folderList .removeAll()
    if let propertylistSongs = UserDefaults.standard.array(forKey: "folders") as? [[String:Any]] {
        arr_folderList = propertylistSongs.compactMap{ FolderList(dictionary: $0) }
    }
        
   // }
    self.tbl_home .reloadData()

  }
    
    
    func downloadfile(strurl:String,playlistName:String) {
        
        let utilityQueue = DispatchQueue.global(qos: .utility)
        
        
      let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
     //   loadingNotification!.mode = .in
        loadingNotification?.labelText = "Downloading"
        loadingNotification?.detailsLabelText = "0% Complete"
        let pathFull:String = playlistName + ".m3u"
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent(pathFull)
            
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(strurl, to: destination).response { response in
            print(response)
            
            if response.error == nil, let imagePath = response.destinationURL?.path {
             //   let image = UIImage(contentsOfFile: imagePath)
                
                DispatchQueue.main.async {
                    
                        let objfolder = FolderList(nameFolder: playlistName, pathFolder: pathFull, dateCreate: Date(), pathUrl: strurl)
//                    let objfolder = FolderList(nameFolder: playlistName, pathFolder:pathFull , dateCreate: Date(),pathFolder)
                    self.arr_folderList .append(objfolder)
                      self.tbl_home .reloadData()
                        //self .fetchData()
                     //   hud .dismiss()
                    MBProgressHUD.hide(for: self.view, animated: true)
                        self.saveData()
                     }
                
                print(imagePath)
            }
            }.downloadProgress { (progress) in
                print("Download Progress: \(progress.fractionCompleted)")
                let double = progress.fractionCompleted * 10000
                
                let intcomplted = Int(progress.fractionCompleted * 100 )
                
                DispatchQueue.main.async {
                  //  hud.progress = Float(intcomplted)
                    //hud.detailTextLabel.text = "\(intcomplted)% Complete"
                    loadingNotification?.detailsLabelText = "\(intcomplted)% Complete"
                    
                    
                }
        }
        
     

    }
       
    //MARK:UIButton Method
    @objc func btnActionEditClicked(_ sender: UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to:self.tbl_home)
       let indexPathGlobal = self.tbl_home.indexPathForRow(at: buttonPosition)!
        
         var objmarklistdata:FolderList = self.arr_folderList[indexPathGlobal.row]as FolderList
    
        
        let alertController = UIAlertController(title: "Edit PlayList Name", message: nil, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "Save", style: .default) { (action) in
            
            
            
            
            let arrNameFolder = self.arr_folderList.map { $0.nameFolder }
            
            if (alertController.textFields?.first?.text?.isContainText())! == false{
                self .showAlertMessage(message:"Please enter playlist name")
                //                TinyToast.shared.show(message: "Please enter playlist name", valign: .center, duration: .normal)
            }else if(alertController.textFields?.last?.text?.isContainText())! == false{
                self .showAlertMessage(message:"Please enter playlist url")
                // TinyToast.shared.show(message: "Please enter playlist url", valign: .center, duration: .normal)
            }
            else if arrNameFolder .contains((alertController.textFields?.first?.text!)!){
                
                self .showAlertMessage(message:"Playlist name allready exist")
                // TinyToast.shared.show(message: "Playlist name allready exist", valign: .center, duration: .normal)
            }
            else {
                
                self.arr_folderList .remove(at: indexPathGlobal.row)
                self.tbl_home .reloadData()
                self.downloadfile(strurl: (alertController.textFields?.last?.text)!, playlistName: (alertController.textFields?.first?.text)!)
            }
            
            
//            if (alertController.textFields?.first?.text?.isContainText())! == false{
//                self .showAlertMessage(message:"Please enter playlist name")
//            }else {
//               objmarklistdata.nameFolder = (alertController.textFields?.first?.text!)!
//               self.arr_folderList[indexPathGlobal.row]  = objmarklistdata
//               self .saveData()
//            }
            
        }
        alertController .addTextField(configurationHandler: { (textfield) in
            textfield.text = objmarklistdata.nameFolder
        })
        alertController .addTextField(configurationHandler: { (textfield) in
            textfield.text = objmarklistdata.pathUrl
        })
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action) in
            
        }
        alertController .addAction(actionOK)
        alertController .addAction(actionCancel)
        
        self.present(alertController, animated: false, completion: nil)
    }
    @IBAction func segment_changed(_ sender: HMSegmentedControl) {
        
        if sender.selectedSegmentIndex == 0{
         
          //  self.clc_home.isHidden = false
          //  self.clc_home .reloadData()
            self.tbl_home .reloadData()
        }else if sender.selectedSegmentIndex == 1{
          //  self.clc_home.isHidden = false
          //  self.clc_home .reloadData()
              self.tbl_home .reloadData()
        }else {
          //  self.tbl_home.isHidden = false
          //  self.clc_home.isHidden = true
            
            self.arr_RemotefolderList .removeAllObjects()
            self.arr_RemotefolderList = self.arr_OriginalRemotefolderList.mutableCopy() as! NSMutableArray
            self.tbl_home .reloadData()
        }
        
    }
    @objc func btnActionDeleteClicked(_ sender: UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to:self.tbl_home)
        let indexPathGlobal = self.tbl_home.indexPathForRow(at: buttonPosition)!
         //var objmarklistdata:FolderList = self.arr_folderList[indexPathGlobal.row]as FolderList
        
        let alertController = UIAlertController(title: "Are you sure,you want to delete this PlayList ?", message: nil, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "YES", style: .default) { (action) in
            
            self.arr_folderList .remove(at: indexPathGlobal.row)
            self.saveData()
            self.tbl_home .reloadData()
        }
        
        let actionCancel = UIAlertAction(title: "NO", style: .default) { (action) in
            
        }
        alertController .addAction(actionOK)
        alertController .addAction(actionCancel)
        
        self.present(alertController, animated: false, completion: nil)
    }

    @IBAction func btnActionPlusClicked(_ sender: UIButton) {
        
//        user_id
//        name
        
          /*
        if self.isfromRemote {
            
            let  alertControllerM3U = UIAlertController(title: "Remote Playlist Name", message: nil, preferredStyle: .alert)
            alertControllerM3U .addTextField(configurationHandler: { (textfield) in
                textfield.placeholder = "Playlist Name";
            })
            let actionOK = UIAlertAction(title: "Save", style: .default) { (action) in
  
                if (alertControllerM3U.textFields?.first?.text?.isContainText())! == false {
                    self .showAlertMessage(message:"Please enter playlist name")
                    
                }
                else {
                    self .submitPlayListName(name: (alertControllerM3U.textFields?.first?.text)!)
                }
                
            }
            let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action) in
                
            }
            alertControllerM3U .addAction(actionOK)
            alertControllerM3U .addAction(actionCancel)
            
            self.present(alertControllerM3U, animated: true, completion: nil)
         
        }
        else {
         */
         let alertController = UIAlertController(title: "Select Type", message: nil, preferredStyle: .alert)
        let actionM3u = UIAlertAction(title: "M3U URL", style: .default) { (action) in
            
            let  alertControllerM3U = UIAlertController(title: "Select Type", message: nil, preferredStyle: .alert)
            alertControllerM3U .addTextField(configurationHandler: { (textfield) in
                textfield.placeholder = "Playlist Name";
        //textfield.text = "demo"
                
            })
            alertControllerM3U .addTextField(configurationHandler: { (textfield) in
                 textfield.placeholder = "Playlist Link";
               //textfield.text = "http://portals.enjoy-iptv.com:8080/get.php?username=M10jT17pMD&password=5LxJBVA9be&type=m3u_plus&output=ts"
                textfield.text = "http://web24.live:8080/get.php?username=2493496367&password=xrhJ1Dgy9U&type=m3u_plus&output=ts"
            })
            
            let actionOK = UIAlertAction(title: "Save", style: .default) { (action) in
                
                let arrNameFolder = self.arr_folderList.map { $0.nameFolder }
                
                if (alertControllerM3U.textFields?.first?.text?.isContainText())! == false {
                       self .showAlertMessage(message:"Please enter playlist name")
                }else if(alertControllerM3U.textFields?.last?.text?.isContainText())! == false {
                      self .showAlertMessage(message:"Please enter playlist url")
                }
                else if arrNameFolder .contains((alertControllerM3U.textFields?.first?.text!)!){
                     self .showAlertMessage(message:"Playlist name allready exist")
                }
                else {
                    let objfolder = FolderList(nameFolder: (alertControllerM3U.textFields?.first?.text)!, pathFolder: "", dateCreate: Date(), pathUrl: (alertControllerM3U.textFields?.last?.text)!)
                    self.arr_folderList .append(objfolder)
                    self.tbl_home .reloadData()
                    
                    self.saveData()
                 
                }
 
            }
            let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action) in
                
            }
            alertControllerM3U .addAction(actionOK)
            alertControllerM3U .addAction(actionCancel)
                
           self.present(alertControllerM3U, animated: true, completion: nil)
            
            
        }
        let actionjson = UIAlertAction(title: "JSON URL", style: .default) { (action) in
            
        }
        let actionCancel = UIAlertAction(title: "CANCEL", style: .cancel) { (action) in
            
        }
        alertController .addAction(actionM3u)
        alertController .addAction(actionjson)
        alertController .addAction(actionCancel)
        self.present(alertController, animated: true, completion: nil)
        
    }
    func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .portrait
    }
    override var shouldAutorotate: Bool {
        return false
    }

}
extension HomeVC :GADBannerViewDelegate
{
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
/*
extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if self.segment_home.selectedSegmentIndex == 0 {
              return self.arr_tvShowList.count
        }
       else if self.segment_home.selectedSegmentIndex == 1 {
            return self.arr_movieList.count
        }
       return 0
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
         if self.segment_home.selectedSegmentIndex == 0 {
        return (self.arr_tvShowList[section] as AnyObject).count
        }else if self.segment_home.selectedSegmentIndex == 1 {
             return (self.arr_movieList[section] as AnyObject).count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let imgvw_banner = cell.contentView.viewWithTag(10) as! UIImageView
           let lbl_name = cell.contentView.viewWithTag(11) as! UILabel
        
         var dict = NSDictionary()
          if self.segment_home.selectedSegmentIndex == 0 {
            let array = self.arr_tvShowList[indexPath.section] as! NSArray
             dict = array[indexPath.row] as! NSDictionary
              lbl_name.text = dict .value(forKey: "series_name") as? String
            
          }else if self.segment_home.selectedSegmentIndex == 1 {
            let array = self.arr_movieList[indexPath.section] as! NSArray
             dict = array[indexPath.row] as! NSDictionary
            
             lbl_name.text = dict .value(forKey: "name") as? String
         }
       
        if let coverImage = dict .value(forKey: "cover_image")as? String {
            imgvw_banner.sd_setImage(with: URL(string:coverImage), completed: { (image, error, sdimageCache, url) in
            })
        }
        
    
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.segment_home.selectedSegmentIndex == 0{
            let array = self.arr_tvShowList[indexPath.section] as! NSArray
            let dict = array[indexPath.row] as! NSDictionary
            let objtvdetail = self.storyboard?.instantiateViewController(withIdentifier: "TVShowDetailVC") as! TVShowDetailVC
            objtvdetail.dict_detail = dict
            self.navigationController?.pushViewController(objtvdetail, animated: false)
        }else{
            let array = self.arr_movieList[indexPath.section] as! NSArray
            let dict = array[indexPath.row] as! NSDictionary
            
            let strVideo = dict .value(forKey: "movie_video") as! String
            let obj = WMPlayerModel()
            obj.title = "";
            obj.videoURL = URL(string: strVideo)
            
            let extns =  obj.videoURL.pathExtension
            if (extns.count > 0) {
                let objDetailPlayer = DetailViewController()
                objDetailPlayer.playerModel = obj
                self.navigationController?.pushViewController(objDetailPlayer, animated: true)
            }else {
                let objDetailPlayer = WNPlayerDetailViewController()
                objDetailPlayer.playerModel = obj;
                self.navigationController?.pushViewController(objDetailPlayer, animated: true)
                
            }
        }
  
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var dict = NSDictionary()
        if self.segment_home.selectedSegmentIndex == 0 {
            let array = self.arr_tvShowList[indexPath.section] as! NSArray
            dict = array[indexPath.row] as! NSDictionary
            
        }else if self.segment_home.selectedSegmentIndex == 1 {
            let array = self.arr_movieList[indexPath.section] as! NSArray
            dict = array[indexPath.row] as! NSDictionary
        }
        
        
        if let imagesize = dict .value(forKey: "image_appearance") as? String {
            if imagesize.lowercased() == "small"{
                let cellSize = CGSize(width: (collectionView.bounds.width - (2 * 10))/3, height: 130)
                return cellSize
            }
            else if imagesize.lowercased() == "medium"{
                let cellSize = CGSize(width: (collectionView.bounds.width - (2 * 10))/2, height: 130)
                return cellSize
            }else {
                let cellSize = CGSize(width:collectionView.bounds.width , height: 130)
                return cellSize
            }
            
        }
        return CGSize(width: 0, height: 0)
       
    }
    func collectionView(_ collectionView: UICollectionView,viewForSupplementaryElementOfKind kind: String,
    at indexPath: IndexPath) -> UICollectionReusableView {
    
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "clcheader", for: indexPath)
    switch kind {
        
    case UICollectionView.elementKindSectionHeader:
  
        var dict = NSDictionary()
        if self.segment_home.selectedSegmentIndex == 0 {
            if let array = self.arr_tvShowList[indexPath.section] as? NSArray,array.count > 0 {
                 dict = array[0] as! NSDictionary
            }
        }
        else if self.segment_home.selectedSegmentIndex == 1 {
            if let array = self.arr_movieList[indexPath.section] as? NSArray,array.count > 0 {
                dict = array[0] as! NSDictionary
            }
        }
    
   
         let lbl = headerView .viewWithTag(10) as? UILabel
          lbl?.text = dict .value(forKey: "category_name")as? String
        headerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        return headerView
        
    default:
    assert(false, "Unexpected element kind")
    }
      return headerView
    }
}
*/
extension HomeVC : EmptyDataSetSource,EmptyDataSetDelegate {
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        var str = "No Local file added"
        
      //  if self.isfromRemote {
            
            if segment_home.selectedSegmentIndex == 0{
                str = "No TV show added"
              
            }else if segment_home.selectedSegmentIndex == 1{
                  str = "No movies added"
            }else {
                 //str = "No file added"
            }
        //}
        let attrs = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont .systemFont(ofSize: 20)]
        return NSAttributedString(string: str, attributes: attrs)
    }
}
extension HomeVC : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.segment_home.selectedSegmentIndex == 0{
            return self.arr_tvShowList.count
        }
        else if self.segment_home.selectedSegmentIndex == 1{
            return self.arr_movieList.count
        }
        else {
            return 1;
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.segment_home.selectedSegmentIndex == 0{
            if self.arr_tvShowList.count > 0{
                return 1;
            }
            return 0;
           // return self.arr_tvShowList.count
        }
        else if self.segment_home.selectedSegmentIndex == 1{
            if self.arr_movieList.count > 0{
                return 1;
            }
            return 0
            //return self.arr_movieList.count
        }
        else {
          return self.arr_folderList.count
        }
       
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.segment_home.selectedSegmentIndex == 0 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "cellclc", for: indexPath) as! customTblCell
             cell.selectedIndex = 0
             cell.currentSection = indexPath.section
             let array = self.arr_tvShowList[indexPath.section] as! NSArray
            cell.arr_collectionList = NSMutableArray(array: array)
            cell.objViewController = self;
            cell.collectionView .reloadData()
            return cell
        }
        else if self.segment_home.selectedSegmentIndex == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellclc", for: indexPath) as! customTblCell
            cell.selectedIndex = 1
            cell.currentSection = indexPath.section
               let array = self.arr_movieList[indexPath.section] as! NSArray
            cell.arr_collectionList = NSMutableArray(array: array) //self.arr_movieList
            cell.objViewController = self;
            cell.collectionView .reloadData()
            return cell
        }
        else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let lblName = cell.contentView.viewWithTag(10) as? UILabel
 
        lblName?.text = self.arr_folderList[indexPath.row].nameFolder
        let btnedit = cell.contentView.viewWithTag(20) as? UIButton
        let btndelete = cell.contentView.viewWithTag(21) as? UIButton
        btnedit?.addTarget(self, action: #selector(btnActionEditClicked(_:)), for: .touchUpInside)
        
        btndelete?.addTarget(self, action: #selector(btnActionDeleteClicked(_:)), for: .touchUpInside)

        
        return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.segment_home.selectedSegmentIndex == 2 {
        let utilityQueue = DispatchQueue.global(qos: .utility)
       let objfolder = self.arr_folderList[indexPath.row]
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        //   loadingNotification!.mode = .in
        loadingNotification?.labelText = "Downloading"
        loadingNotification?.detailsLabelText = "0% Complete"
        let pathFull:String = objfolder.nameFolder + ".m3u"
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent(pathFull)
            
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
    
        
        Alamofire.download(objfolder.pathUrl, to: destination).response { response in
            print(response)
            
            if response.error == nil, let imagePath = response.destinationURL?.path {
                //   let image = UIImage(contentsOfFile: imagePath)
                
                DispatchQueue.main.async {
                      do{
                        if let string = try String(contentsOfFile: imagePath, encoding: .utf8) as? String
                       {
                        let channelarr = self .extractChannelsFromRawString(string)
                        //  print(channelarr)
                        
                        MBProgressHUD.hide(for:self.view, animated: true)
                        if channelarr .count > 0{
                            let objgroup = self.storyboard?.instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
                            objgroup.arr_channelsAll = channelarr;
                            self.navigationController?.pushViewController(objgroup, animated: false)
                        }
                        else{
                            self .showAlertMessage(message: "Channel parsing error")
                        }
                        }
                    }
                      catch{
                        MBProgressHUD.hide(for:self.view, animated: true)
                                        print("copy failure.")
                                    }
                    MBProgressHUD.hide(for: self.view, animated: true)
                   // self.saveData()
                }
                
                print(imagePath)
            }
            }.downloadProgress { (progress) in
                print("Download Progress: \(progress.fractionCompleted)")
                let double = progress.fractionCompleted * 10000
                
                let intcomplted = Int(progress.fractionCompleted * 100 )
                
                DispatchQueue.main.async {
                    //  hud.progress = Float(intcomplted)
                    //hud.detailTextLabel.text = "\(intcomplted)% Complete"
                    loadingNotification?.detailsLabelText = "\(intcomplted)% Complete"
                    
                    
                }
        }
        }
 
      }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.segment_home.selectedSegmentIndex == 2 {
            return 50
        }
        
        if indexPath.section == 0 {
            
            if self.segment_home.selectedSegmentIndex == 0 {
                 let array = self.arr_tvShowList[indexPath.row] as! NSArray
                if array.count > 0{
                    return self.view.frame.height/2
                }
              
            }
            else if self.segment_home.selectedSegmentIndex == 1 {
                let array = self.arr_movieList[indexPath.row] as! NSArray
                if array.count > 0{
                    return self.view.frame.height/2
                }
            }
        }
        
        
        
        return 150
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
          let cell = tableView.dequeueReusableCell(withIdentifier: "cellheader")
          var dict = NSDictionary()
            if  self.segment_home.selectedSegmentIndex == 0 {
                if let array = arr_tvShowList[section] as? NSArray,array.count > 0 {
                    dict = array[0] as! NSDictionary
                }
            }
            else if self.segment_home.selectedSegmentIndex == 1 {
                if let array = arr_movieList[section] as? NSArray,array.count > 0 {
                    dict = array[0] as! NSDictionary
                }
            }

        let lbl = cell?.contentView .viewWithTag(10) as? UILabel
        lbl?.text = dict .value(forKey: "category_name")as? String
        cell?.contentView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        return cell?.contentView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.segment_home.selectedSegmentIndex == 2 {
            return 0
        }
        return 40
    }
   
}

extension Array where Element: Hashable {
    var setValue: Set<Element> {
        return Set<Element>(self)
    }
}
